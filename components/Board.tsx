import { MosaicTile } from '@prisma/client';
import useSWR from 'swr';
import Tile from './Tile';

const Board = () => {
  const fetcher: (url: RequestInfo) => Promise<any> = (url) =>
    fetch(url).then((r) => r.json());

  const { data, error } = useSWR<MosaicTile[]>('/api/tiles', fetcher, {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  //   Count the number of X and Y tiles
  const countX = data.filter((tile) => tile.x === 0).length;
  const countY = data.filter((tile) => tile.y === 0).length;

  return (
    <div
      style={{
        border: '1px solid black',
        display: 'grid',
        gridTemplateColumns: `repeat(${countX}, 1fr)`,
        gridTemplateRows: `repeat(${countY}, 1fr)`,
        width: '1200px',
        height: '1000px',
      }}
    >
      {data.map((tile, index) => (
        <Tile key={index} {...tile} />
      ))}
    </div>
  );
};

export default Board;
