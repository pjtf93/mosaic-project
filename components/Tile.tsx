import { MosaicTile } from '@prisma/client';
import { useState } from 'react';
import useSWR from 'swr';

const Tile = ({ x, y, color, userIp }: MosaicTile) => {
  const [currentColor, setCurrentColor] = useState(color);
  const [currentIp, setCurrentIp] = useState(userIp);

  const updateTile = async (x: number, y: number, color: string) => {
    await fetch('/api/update-tile', {
      method: 'POST',
      body: JSON.stringify({ x, y, color }),
    });
  };

  const { mutate } = useSWR(
    `/api/get-tile?x=${x}+&y=${y}`,
    (url) =>
      fetch(url)
        .then((r) => r.json())
        .then((data) => {
          setCurrentColor(data.color);
          setCurrentIp(data.userIp);
        }),
    {
      revalidateOnMount: false,
      revalidateOnFocus: false,
      revalidateIfStale: false,
      refreshInterval: 100,
    }
  );

  const isWhite = currentColor === 'ffffff';

  return (
    <div
      style={{
        border: '1px solid black',
        backgroundColor: `#${currentColor}`,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: '14px',
      }}
      onClick={async () => {
        if (isWhite) {
          await updateTile(x, y, '000000');
          mutate();
        } else {
          await updateTile(x, y, 'ffffff');
          mutate();
        }
      }}
    >
      <span
        style={{
          color: isWhite ? 'black' : 'white',
        }}
      >
        ({x},{y})
      </span>
      <span
        style={{
          color: isWhite ? 'black' : 'white',
        }}
      >
        User IP: <br />
        {currentIp ? currentIp : 'No IP'}
      </span>
    </div>
  );
};

export default Tile;
