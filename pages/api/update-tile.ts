// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { MosaicTile } from '@prisma/client';
import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '../../lib/prisma';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<MosaicTile>
) {
  // Parse the request body to JSON
  const body = JSON.parse(req.body);
  const { x, y, color } = body;
  const ip = req.headers['x-forwarded-for'] as string;

  const tile = await prisma.mosaicTile.update({
    where: {
      x_y: {
        x: x,
        y: y,
      },
    },
    data: {
      color,
      userIp: ip,
    },
  });

  res.status(200).json(tile);
}
