// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { MosaicTile } from '@prisma/client';
import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '../../lib/prisma';

type ApiResponse = MosaicTile | { error: string };

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ApiResponse>
) {
  const { x, y } = req.query;

  const tile = await prisma.mosaicTile.findUnique({
    where: {
      x_y: {
        x: parseInt(x as string),
        y: parseInt(y as string),
      },
    },
  });

  if (!tile) {
    res.status(404).json({ error: 'Tile not found' });
  } else {
    res.status(200).json(tile);
  }
}
