// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { MosaicTile } from '@prisma/client';
import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '../../lib/prisma';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<MosaicTile[]>
) {
  const tiles = await prisma.mosaicTile.findMany({
    orderBy: {
      createdAt: 'asc',
    },
  });

  res.status(200).json(tiles);
}
